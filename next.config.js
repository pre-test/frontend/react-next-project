// next.config.js
const withCSS = require('@zeit/next-css')
const withSass = require('@zeit/next-sass')
module.exports = withCSS( withSass({
  // cssModules: true,
  // optimizeImagesInDev: false,
  sass: true,
  cssLoaderOptions: {
    importLoaders: 1,
    localIdentName: "[local]"
  },
  devIndicators: {
    autoPrerender: false
  }
}))
