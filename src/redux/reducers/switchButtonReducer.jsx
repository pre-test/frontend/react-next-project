import { SWITCH_BUTTON } from '../actions/switchButtonActions';
const switchButtonReducer = (state = { gridSwitch: true }, action) => {
  switch (action.type) {
    case SWITCH_BUTTON:
      return {...state, gridSwitch: action.gridSwitch};
    default: return {...state};
  }
}

export default switchButtonReducer;
