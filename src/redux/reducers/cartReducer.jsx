import { ITEM_COUNTER } from '../actions/cartActions';
const cartReducer = (state = { itemList: [] }, action) => {
  switch (action.type) {
    case ITEM_COUNTER:
      return {...state, itemList: state.itemList.length > 0 ? state.itemList.concat(action.itemCounter) : action.itemCounter };
    default: return {...state};
  }
}

export default cartReducer;
