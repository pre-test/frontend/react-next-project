import cartReducer from './cartReducer';
import switchButtonReducer from './switchButtonReducer';
import {combineReducers} from 'redux';

const rootReducer = combineReducers({
  cartData: cartReducer,
  switchData: switchButtonReducer
});

export default rootReducer;
