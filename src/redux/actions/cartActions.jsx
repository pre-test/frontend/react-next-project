export const ITEM_COUNTER = "ITEM_COUNTER";

// export const setCart = (itemCounter) => ({
//   type: ITEM_COUNTER,
//   itemCounter
// });

export const setCart = (itemCounter) => dispatch => {
  return dispatch({
    type: ITEM_COUNTER,
    itemCounter
  })
}
