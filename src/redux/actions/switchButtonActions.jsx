export const SWITCH_BUTTON = "SWITCH_BUTTON";

export const setSwitchButton = (gridSwitch) => dispatch => {
  return dispatch({
    type: SWITCH_BUTTON,
    gridSwitch
  })
}
