export default function separateNumber(
    val,
    $toFixed,
    type = 0,
    negativeNum = false
) {
  try {
    if (typeof $toFixed === 'undefined') {
      if (parseFloat(val) > 0 || negativeNum) {
        while (/(\d+)(\d{3})/.test(val.toString())) {
          val = val.toString().replace(/(\d+)(\d{3})/, '$1,$2')
        }
        return val
      }
      return type
    } else if (typeof $toFixed === 'number') {
      if (parseFloat(val) > 0 || negativeNum) {
        let num = parseFloat(val).toFixed($toFixed)
        num += ''
        const x = num.split('.')
        let x1 = x[0]
        const x2 = x.length > 1 ? '.' + x[1] : ''
        const rgx = /(\d+)(\d{3})/
        while (rgx.test(x1)) {
          x1 = x1.replace(rgx, '$1,$2')
        }
        return x1 + x2
      }
      return type
    }
    return val
  } catch (e) {
    return type
  }
}
