import Document, { Head, Main, NextScript } from 'next/document'
import Navbar from '../components/standard-layout/navbar'
import Footer from '../components/standard-layout/footer'
export default class MyDocument extends Document {
  render() {
    return (
      <html>
        <Head>
          <meta name="viewport" content="width=device-width, initial-scale=1" />
          <link rel="shortcut icon" href="/static/favicon.ico" type="image/x-icon" />
        </Head>
        <body>
          <Navbar />
          <Main />
          <NextScript />
          <Footer />
        </body>
      </html>
    );
  }
}
