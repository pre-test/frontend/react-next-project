import { useState, useEffect } from "react"
import gridCard from "../components/product/gridCard"
import listCard from "../components/product/listCard"
import { ButtonGroup, Button } from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faThLarge, faListUl } from '@fortawesome/free-solid-svg-icons'
import axios from 'axios'
import {bindActionCreators} from "redux";
import {setSwitchButton} from "../redux/actions/switchButtonActions";
import {connect} from "react-redux";
const product = (props) => {
  const components = {
    gridCard: gridCard,
    listCard: listCard
  };
  const [switchButton, setSwitchButton] = useState(true);
  let [productList, setProductList] = useState([]);
  const setSwitch = (value) => {
    props.setSwitchButton(value)
  }
  useEffect(() => {
    setSwitchButton(props.switchData.gridSwitch)
    setProductList(props.productData)
  }, [props.switchData.gridSwitch, props.productData]);
  return (
    <article className="container">
      <div id="switchBox">
        <ButtonGroup>
          <Button className="btn-standard btn-custom" onClick={()=>setSwitch(true)} active={switchButton}>
            <FontAwesomeIcon icon={faThLarge}></FontAwesomeIcon>
          </Button>
          <Button className="btn-standard btn-custom" onClick={()=>setSwitch(false)} active={!switchButton}>
            <FontAwesomeIcon icon={faListUl}></FontAwesomeIcon>
          </Button>
        </ButtonGroup>
      </div>
      <section className="row">
        { switchButton ?
          productList.map((product, productIndex) =>
            <div className="col-sm-6 col-md-4 col-lg-3 mt-4" key={product._id}>
              <components.gridCard cardData={product} />
            </div>
          ) :
          productList.map((product, productIndex) =>
            <div className="col-12 mt-4 pl-5 pr-5" key={product._id}>
              <components.listCard cardData={product} />
            </div>
          )
        }
      </section>
      <style jsx>
        {`
          #switchBox {
            text-align: right;
            border-bottom: 2px solid #f4f4f4;
            padding-bottom: 10px;
          }
        `}
      </style>
      <style global jsx>
        {`
          .btn-custom {
            background: white !important;
            border-color: #cfcfcf !important;
            color: black;
            &:hover {
              color: black;
            }
          }
        `}
      </style>
    </article>
  );
}

product.getInitialProps = async (ctx) => {
  let productData = []
  try {
    await axios.get('https://cc-mock-api.herokuapp.com/product')
        .then(res => {
          productData = res.data
        })
        .catch(error => {
          console.log(error)
        })
  } catch (error) {
    console.error(error)
  }

  return {
    productData
  }
}

const mapStateToProps = (state) => ({
  switchData: state.switchData
});

const mapDispatchToProps = dispatch => ({
  setSwitchButton: bindActionCreators(setSwitchButton, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(product);
