import detailContent from "../../components/product/detail/detailContent"
import { useState, useEffect } from "react"
import { Button } from 'reactstrap'
import Link from 'next/link'
import axios from "axios";
const productDetail = (props) => {
  const components = {
    detailContent: detailContent
  }
  const [detailData, setDetailData] = useState('')
  useEffect(() => {
    setDetailData(props.detailData)
  }, [props.detailData])
  return (
    <article className="container">
      <nav aria-label="breadcrumb">
        <ol className="breadcrumb">
          <li className="breadcrumb-item"><Link href="/" prefetch={false}><a>Home</a></Link></li>
          <li className="breadcrumb-item active" aria-current="page">{detailData.name}</li>
        </ol>
      </nav>
      <section>
        <components.detailContent detailData={detailData} />
      </section>
    </article>
  );
}

productDetail.getInitialProps = async (context) => {
  let detailData = {}
  const productId = context.query.productId
  try {
    await axios.get(`https://cc-mock-api.herokuapp.com/product/${productId}`)
      .then(res => {
        detailData = res.data
      })
      .catch(error => {
        console.log(error)
      })
  } catch (error) {
    console.error(error)
  }
  return {detailData}
}

/*export async function getServerSideProps(context) {
  let detailData = {}
  const productId = context.query.productId
  try {
    await axios.get(`https://cc-mock-api.herokuapp.com/product/${productId}`)
      .then(res => {
        detailData = res.data
      })
      .catch(error => {
        console.log(error)
      })
  } catch (error) {
    console.error(error)
  }
  return { props: {detailData} }
}*/
export default productDetail
