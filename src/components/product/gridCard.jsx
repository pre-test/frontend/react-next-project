import { useState } from "react"
import Router from 'next/router'
import separateNumber from '../../utility'
import { Card, CardImg, CardBody, Button } from 'reactstrap';
export default (props) => {
  const [cardData, setCardData] = useState(props.cardData)
  const linkToProductDetail = () => {
    Router.push('/product-detail/[productId]', `/product-detail/${cardData._id}`)
  }
  return (
    <div id="cardBox">
      <Card>
        <div className="big-image-box">
          { cardData.image_url ? <CardImg top width="100%" className="img-fluid" src={cardData.image_url} alt="Card image cap" />
              : <CardImg top className="img-fluid" width="100%" src="static/images/no-image.png" alt="Card image cap" />
          }
        </div>
        <CardBody>
          <div className="title-box">
            { cardData.brand_info.url ? <img className="small-img" src={cardData.brand_info.url} alt="Small image" />
                : <img className="small-img" src="/static/images/no-image.png" alt="Small image" />
            }
            <h5 className="title-text">{cardData.name}</h5>
          </div>
          <p className="description-text">{cardData.description}</p>
          <p className="price-text">฿{separateNumber(cardData.price)}</p>
          <Button className="btn-standard col-12 mt-4" onClick={linkToProductDetail}>Detail</Button>
        </CardBody>
      </Card>
      <style jsx>
        {`
          #cardBox {
            border-radius: .25rem;
            box-shadow: 0 0 3px #dddddd;
            transition: all .3s ease-in-out;
            &:hover {
              border-radius: .25rem;
              box-shadow: 0 0 30px #b8b8b8;
            }
            .big-image-box {
              height: 200px;
              display: flex;
            }
            .title-box {
              display: flex;
              align-items: center;
              .small-img {
                width: 20%;
                height: 20%;
                border: 1px solid #ebebeb;
                border-radius: 5px;
                margin-right: 10px;
              }
            }
            .title-text, .description-text {
              height: 45px;
              overflow: hidden;
              display: -webkit-box;
              -webkit-line-clamp: 2;
              -webkit-box-orient: vertical;
            }
            .price-text {
              text-align: right;
              font-weight: 700;
              color: #ff4878;
            }
          }
        `}
      </style>
    </div>
  );
}
