import { useState } from "react"
import Router from 'next/router'
import separateNumber from '../../utility'
import { CardImg, Button } from 'reactstrap';
export default (props) => {
  const [cardData, setCardData] = useState(props.cardData)
  const linkToProductDetail = () => {
    Router.push('/product-detail/[productId]', `/product-detail/${cardData._id}`)
  }
  return (
    <div id="cardBox" className="row">
      <div className="img-card col-12 col-sm-12 col-md-2 p-0">
        <div className="big-image-box">
          { cardData.image_url ? <CardImg top width="100%" className="img-fluid custom-img" src={cardData.image_url} alt="Card image cap" />
              : <CardImg top className="img-fluid custom-img" width="100%" src="/static/images/no-image.png" alt="Card image cap" />
          }
        </div>
      </div>
      <div className="content-box col-sm-8 col-md-7 pt-0 pb-0 pr-0">
        <h5 className="title-text">{cardData.name}</h5>
        <label className="description-text">{cardData.description}</label>
      </div>
      <div className="right-box col-sm-4 col-md-3">
        <div className="row">
          <label className="price-text col-md-12 p-0">฿{separateNumber(cardData.price)}</label>
        </div>
        <div className="row">
          <Button className="btn-standard custom-btn col-md-3 ml-auto mt-2" onClick={linkToProductDetail}>Detail</Button>
        </div>
      </div>
      <style jsx>
        {`
          #cardBox {
            display: flex;
            padding: 20px;
            transition: all .3s ease-in-out;
            &:hover {
              border-radius: .25rem;
              box-shadow: 0 0 30px #b8b8b8;
              margin: 0 -2rem !important;
            }
            .img-card {
              border-radius: .25rem;
              border: 1px solid rgba(0,0,0,.125);
              .big-image-box {
                height: 172px;
                display: flex;
              }
            }
            .content-box {
              flex: 1 1 auto;
              min-height: 1px;
              .title-text, .description-text {
                // height: 45px;
                overflow: hidden;
                display: -webkit-box;
                -webkit-line-clamp: 6;
                -webkit-box-orient: vertical;
              }
            }
            .right-box {
              text-align: right;
              align-self: center;
              .price-text {
                text-align: right;
                font-weight: 700;
                color: #ff4878;
              }
            }
          }
        `}
      </style>
      <style global jsx>
        {`
          .custom-img {
            border-radius: .25rem;
          }
          .custom-btn {
            min-width: 62px;
          }
        `}
      </style>
    </div>
  );
}
