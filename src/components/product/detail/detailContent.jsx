import {Card, CardImg, Button, InputGroup, InputGroupAddon, Input} from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPlus, faMinus } from '@fortawesome/free-solid-svg-icons'
import { useState, useEffect } from "react"
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { setCart } from '../../../redux/actions/cartActions'
import separateNumber from '../../../utility'

const detailContent = (props) => {
  const [detailContent, setDetailContent] = useState({})
  const [count, setCount] = useState(1)
  const addItem = () => {
    let cardItemList = []
    for (let i = 1; i <= count; i++) {
      cardItemList.push(detailContent)
    }
    props.setCart(cardItemList)
  }
  useEffect(() => {
    setDetailContent(props.detailData)
  }, [props.detailData])
  return (
    <div id="detailContentBox" className="row">
      <div className="col-sm-12 col-md-4">
        <div className="img-card">
          <div className="big-image-box">
            { detailContent.image_url ? <CardImg top width="100%" className="img-fluid custom-img" src={detailContent.image_url} alt="Card image cap" />
                : <CardImg top className="img-fluid custom-img" width="100%" src="/static/images/no-image.png" alt="Card image cap" />
            }
          </div>
        </div>
      </div>
      <div className="col-sm-12 col-md-8 mt-2">
        <h3>{detailContent.name}</h3>
        <p>{detailContent.description}</p>
        <p className="price-text">฿{separateNumber(detailContent.price)}</p>
        <InputGroup className="custom-input-group-box">
          <InputGroupAddon addonType="prepend" className="custom-input-group">
            <Button onClick={()=> count > 1 ? setCount(count-1) : 1}>
              <FontAwesomeIcon icon={faMinus}></FontAwesomeIcon>
            </Button>
          </InputGroupAddon>
          <Input className="custom-input" value={count} onChange={e => setCount(+e.target.value > 1 ? +e.target.value : 1)} />
          <InputGroupAddon addonType="prepend" className="custom-input-group delete-border">
            <Button onClick={()=>setCount(count+1)}>
              <FontAwesomeIcon icon={faPlus}></FontAwesomeIcon>
            </Button>
          </InputGroupAddon>
        </InputGroup>
        <Button className="btn-standard col-12 col-md-2 mt-4" onClick={addItem}>Add to Cart</Button>
      </div>
      <style jsx>
        {`
          #detailContentBox {
            // display: flex;
            .img-card {
              width: fit-content;
              border-radius: .25rem;
              border: 1px solid rgba(0,0,0,.125);
              .big-image-box {
                max-width: 500px;
                max-height: 400px;
                display: flex;
              }
            }
            .price-text {
              font-weight: 700;
              color: #ff4878;
            }
          }
        `}
      </style>
      <style global jsx>
        {`
          .custom-img {
            border-radius: .25rem;
          }
          .custom-input-group-box {
            width: fit-content;
            .custom-input {
              box-shadow: none;
              width: 60px;
              text-align: center;
              &:focus {
                border-color: #2cc3a7 !important;
              }
            }
            .custom-input-group .btn {
              background: white;
              color: black;
              border-color: #ced4da;
              &:focus {
                box-shadow: none !important;
              }
              &:active {
                background: #2cc3a7 !important;
                border-color: #2cc3a7 !important;
              }
            }
            .delete-border .btn {
              border-left: 0;
            }
          }
          .custom-btn {
            min-width: 62px;
          }
        `}
      </style>
    </div>
  );
}

const mapStateToProps = (state) => ({
  cartData: state.itemList
});

const mapDispatchToProps = dispatch => ({
  setCart: bindActionCreators(setCart, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(detailContent);
