export default (props) => {
  return (
    <footer id="footerBox">
      <div className="footer-container">
        <label><b>Example React and Next</b> by <span className="blue-font">Kitsana Sappakitjarern.</span></label>
        <p className="blue-font">Software Engineer</p>
      </div>
      <style jsx>
        {`
          #footerBox {
            display: block;
            width: 100%;
            flex: none;
            background-color: #F6F6F6;
            .footer-container {
              width: fit-content;
              margin: 50px auto;
              text-align: center;
              .blue-font {
                color: #248ae2;
              }
            } 
          } 
      `}
      </style>
    </footer>
  );
}
