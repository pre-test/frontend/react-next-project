export default (props) => {
  return (
    <navbar id="navbarBox">
      <div className="nav-container">
        <div className="answer-box">Answer</div>
        <label>This is an example of how to development product lists page from UI Design.</label>
      </div>
      <style jsx>
        {`
          #navbarBox { 
            position: fixed;
            top: 0;
            z-index: 99;
            width: 100%;
            height: 80px;
            background-color: #fff;
            box-shadow: 0 0 6px 0 rgba(0,0,0,.4);
            margin-bottom: 2px;
            user-select: none;
            .nav-container {
              margin: auto;
              display: flex;
              align-items: center;
              height: 100%;
              width: fit-content;
              .answer-box {
                background: #2cc3a7;
                border-radius: 5px;
                padding: 5px 10px;
                margin-right: 5px;
                color: white;
              }
            } 
          } 
      `}
      </style>
    </navbar>
  );
}
